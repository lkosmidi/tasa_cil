




open Cil
open Random
module E = Errormsg


open Tututil 


class attrRmVisitor = object(self)
  inherit nopCilVisitor 

  method vattr (a : attribute) = 
    match a with
    | Attr (a, b) ->
      E.log "attribute %s\n" a;
      ChangeTo [Attr ("const", []) ]
      (*SkipChildren*)
    | _ -> SkipChildren 
end


let processDecl (gvar : varinfo) (loc : location) : unit =
  E.log "%a: gloval variable declaration or prototype detected: %s\n" d_loc loc gvar.vname;
  match unrollType gvar.vtype with
  TFun (rt, args, isva, a) ->
    let vis = new attrRmVisitor in
    ignore(visitCilAttributes vis (typeAttrs rt) );
    E.log "return type:%a\n" d_type rt;
    ignore(typeRemoveAttributes ["host"] rt);
    ignore(typeRemoveAttributes ["cudart_builtin"] rt);
    ignore(typeRemoveAttributes ["__cudart_builtin__"] rt);
    ignore(typeRemoveAttributes ["device_builtin"] rt);
    ignore(typeRemoveAttributes ["__device_builtin__"] rt);
    E.log "return type after:%a\n" d_type rt;
  | _ -> ()

let randomise_globals : bool = true
let randomise_functions : bool = true
let randomise_stack : bool = true
let function_definitions = ref []
let gpu_function_definitions = ref []
let init_global_variable_definitions = ref []
let uninit_global_variable_definitions = ref []
let ronly_global_variable_definitions = ref []
(* We keep in this list anything that we don't shuffle, so that their relative
 * ordering is preserved *)
let rest_of_globals : global list ref = ref []

let knuth_shuffle l=
  let a = Array.of_list l in
  let n = Array.length a in
  let a = Array.copy a in
  for i = n -1 downto 1 do
    let k = Random.int (i+1) in
    let x = a.(k) in
    a.(k) <- a.(i);
    a.(i) <- x
  done;
  Array.to_list a

let fixed_shuffle l=
  let a = Array.of_list l in
  let tmp = a.(2) in
    a.(2) <- a.(3);
    a.(3) <- tmp;
  Array.to_list a

(* This pass is invoked at merge time and simply removes __host__, __device__,
 * __global__ attributes introduced in the CUDA prototypes by preprocessing the
 * merged files
 *)
let merge_remove_cuda (f : file) : unit =
begin
  rest_of_globals := [];
  E.log "Remove cuda stuff before we merge\n";
  iterGlobals f ( onlyFunctionDecls ( processDecl ) ) ;
  List.iter (fun g ->
    match g with
    | GVarDecl (gvar, loc) -> begin
      E.log "%a: gloval variable declaration or prototype detected: %s\n" d_loc loc gvar.vname;
      match unrollType gvar.vtype with
      TFun (rt, args, isva, a) ->
              (* __host__ __device__ and __global__ are converted by the
               * preprocessor to attributes, however the support we added to
               * CIL creates parsing errors when they are encountered*)
              (* Let's first remove the problematic attribute *)
              let newrt = typeRemoveAttributes ["host"; "device"; "global";
                           "cudart_builtin"; "device_builtin"] rt in
              (* now change the function prototype *)
              gvar.vtype <- TFun (newrt, args, isva, a);
              E.log "new return type:%a\n" d_type newrt;
              rest_of_globals := g :: !rest_of_globals;
      | _ ->  rest_of_globals := g :: !rest_of_globals;
      end
    | _ ->
      rest_of_globals := g :: !rest_of_globals;
    )
  f.globals;

  (* Now reverse the result and return the resulting file *)
  let rec revonto acc = function
      [] -> acc
    | x :: t -> revonto (x :: acc) t
  in
  f.globals  <- revonto [] !rest_of_globals;
  E.log "done\n";
end

(* nvcc implicitly includes all CUDA header files by default, so it complains
 * about duplicate definitions*
 * This pass removes everything included by CUDA header files, as well as some
 * hacks we introduce in cuda_cil.h such as dummy uint3 definitions for
 * threadIdx, blockIdx and blockDim, so that nvcc compiles the file without the
 * need to manually remove them
 *)
let remove_cuda (f : file) : unit =
begin
  E.log "Remove cuda stuff\n";
  List.iter (fun g ->
    match g with
    | GFun (fd, loc) ->
      E.log "%a: Function detected: %s\n" d_loc loc fd.svar.vname;
      if ( not (Str.string_match (Str.regexp "cuda_headers.*") loc.file 0) &&
           not (Str.string_match (Str.regexp "cuda_cil.*") loc.file 0)
         ) then
      (* Add the function definition in the list *)
      rest_of_globals := g :: !rest_of_globals;
    | GVar (gvar, init, loc) ->
      E.log "%a: global variable definition detected: %s\n" d_loc loc gvar.vname;
      (* Add the global variable definition in the list *)
      if gvar.vname <> "blockIdx" && gvar.vname <> "blockDim" && gvar.vname <> "threadIdx" then
      rest_of_globals := g :: !rest_of_globals;
    | GType (gt, loc) ->
      E.log "%a: typedef detected: %s\n" d_loc loc gt.tname;
      if ((gt.tname <> "size_t") && (gt.tname <> "__off64_t") &&
         ( (gt.tname = "CUcontext") || (gt.tname = "CUdevice") || (gt.tname = "CUresult") || not (Str.string_match (Str.regexp "cuda_headers.*") loc.file 0) ))
      then
      rest_of_globals := g :: !rest_of_globals;
    | GCompTag (gc, loc) ->
      E.log "%a: struct definition detected: %s\n" d_loc loc gc.cname;
      if not (Str.string_match (Str.regexp "cuda_headers.*") loc.file 0) && gc.cname <> "_IO_marker" && gc.cname <> "_IO_FILE" then
      rest_of_globals := g :: !rest_of_globals;
    | GCompTagDecl (gcd, loc) ->
      E.log "%a: struct declaration detected: %s\n" d_loc loc gcd.cname;
      if not (Str.string_match (Str.regexp "cuda_headers.*") loc.file 0) then
      rest_of_globals := g :: !rest_of_globals;
    | GEnumTag (en, loc) ->
      E.log "%a: enum definition detected: %s\n" d_loc loc en.ename;
      if (en.ename == "cudaError_enum") || not (Str.string_match (Str.regexp "cuda_headers.*") loc.file 0) then
      rest_of_globals := g :: !rest_of_globals;
    | GEnumTagDecl (en, loc) ->
      E.log "%a: enum declaration detected: %s\n" d_loc loc en.ename;
      rest_of_globals := g :: !rest_of_globals;
    | GVarDecl (gvar, loc) ->
      E.log "%a: gloval variable declaration or prototype detected: %s\n" d_loc loc gvar.vname;
      if ( gvar.vname <> "__threadfence" && gvar.vname <> "malloc" && gvar.vname <> "free" && gvar.vname <> "memset" && gvar.vname <> "clock" && 
      (
           (gvar.vname = "cuInit") || (gvar.vname = "cuDeviceGet") || (gvar.vname = "cuCtxCreate_v2") ||
           not (Str.string_match (Str.regexp "cuda_headers.*") loc.file 0) &&
           not (Str.string_match (Str.regexp "cuda_cil.*") loc.file 0)
      )
         ) then
      rest_of_globals := g :: !rest_of_globals;
    | GAsm (asm, loc) ->
      E.log "%a: gloval asm stagement detected: %s\n" d_loc loc asm;
      rest_of_globals := g :: !rest_of_globals;
    | GPragma (pr, loc) ->
      E.log "%a: top level pragma detected: %a\n" d_loc loc d_attr pr;
      rest_of_globals := g :: !rest_of_globals;
    | GText (txt) ->
      E.log "top comment detected: %s\n" txt;
      rest_of_globals := g :: !rest_of_globals;
    | _ ->
      E.s (bug "unhandled global type, abort");
    )
  f.globals;

  (* Now reverse the result and return the resulting file *)
  let rec revonto acc = function
      [] -> acc
    | x :: t -> revonto (x :: acc) t
  in
  f.globals  <- revonto [] !rest_of_globals;
end


let tut16 (f : file) : unit =
  (* funvar |> processFunction |> onlyFunctions |> iterGlobals f *)
  (* iterGlobals f ( onlyFunctions ( processFunction ( funvar ) ) ) *)
  (* knuth_shuffle f.globals; *)
  (* let shuffled_globals = fixed_shuffle f.globals in
  List.iter (fun g ->
          match g with
          | _ -> E.log "printing: %a \n" d_global g;
            ()
  )
  shuffled_globals;

  f.globals <- shuffled_globals;
*)
  Random.self_init();
  List.iter (fun g ->
    match g with
    | GFun (fd, loc) ->
      E.log "%a: Function detected: %s\n" d_loc loc fd.svar.vname;
      let locals' = if randomise_stack then knuth_shuffle fd.slocals else fd.slocals in
      fd.slocals <- locals';
      (* Add the function definition in the gpu or host function list *)
      if (fd.svar.vglobalkernel == true) || (fd.svar.vdevicekernel == true) then begin
         gpu_function_definitions := g :: !gpu_function_definitions; end
      else
         function_definitions := g :: !function_definitions;
    | GVar (gvar, init, loc) ->
      begin
         E.log "%a: global variable definition detected: %s\n" d_loc loc gvar.vname;
         (* Add the global variable definition in the readonly, initialised or
          * uninitialised list *)
         let is_const vi = hasAttribute "const" (typeAttrs vi.vtype) in
         if is_const gvar then begin
            ronly_global_variable_definitions := g :: !ronly_global_variable_definitions;
         end
         else begin
            match gvar.vinit.init with
            None -> uninit_global_variable_definitions := g :: !uninit_global_variable_definitions;
            | Some i -> init_global_variable_definitions := g :: !init_global_variable_definitions;
         end
      end
    | GType (gt, loc) ->
      E.log "%a: typedef detected: %s\n" d_loc loc gt.tname;
      rest_of_globals := g :: !rest_of_globals;
    | GCompTag (gc, loc) ->
      E.log "%a: struct definition detected: %s\n" d_loc loc gc.cname;
      rest_of_globals := g :: !rest_of_globals;
    | GCompTagDecl (gcd, loc) ->
      E.log "%a: struct declaration detected: %s\n" d_loc loc gcd.cname;
      rest_of_globals := g :: !rest_of_globals;
    | GEnumTag (en, loc) ->
      E.log "%a: enum definition detected: %s\n" d_loc loc en.ename;
      rest_of_globals := g :: !rest_of_globals;
    | GEnumTagDecl (en, loc) ->
      E.log "%a: enum declaration detected: %s\n" d_loc loc en.ename;
      rest_of_globals := g :: !rest_of_globals;
    | GVarDecl (gvar, loc) -> begin
      E.log "%a: gloval variable declaration or prototype detected: %s\n" d_loc loc gvar.vname;
      match unrollType gvar.vtype with
      TFun (rt, args, isva, a) ->
              (* __host__ __device__ and __global__ are converted by the
               * preprocessor to attributes, however the support we added to
               * CIL creates parsing errors when they are encountered*)
              (* Let's first remove the problematic attribute *)
              let newrt = typeRemoveAttributes ["host"; "device"; "global"] rt in
              (* now change the function prototype *)
              gvar.vtype <- TFun (newrt, args, isva, a);
              E.log "return type:%a\n" d_type newrt;
              rest_of_globals := g :: !rest_of_globals;
      | _ ->  rest_of_globals := g :: !rest_of_globals;
      end
    | GAsm (asm, loc) ->
      E.log "%a: gloval asm stagement detected: %s\n" d_loc loc asm;
      rest_of_globals := g :: !rest_of_globals;
    | GPragma (pr, loc) ->
      E.log "%a: top level pragma detected: %a\n" d_loc loc d_attr pr;
      rest_of_globals := g :: !rest_of_globals;
    | GText (txt) ->
      E.log "top comment detected: %s\n" txt;
      rest_of_globals := g :: !rest_of_globals;
    | _ ->
      E.s (bug "unhandled global type, abort");
    )
  f.globals;

  let ronly_global_variable_definitions' = if randomise_globals then knuth_shuffle !ronly_global_variable_definitions else !ronly_global_variable_definitions in
  let init_global_variable_definitions' = if randomise_globals then knuth_shuffle !init_global_variable_definitions else !init_global_variable_definitions in
  let uninit_global_variable_definitions' = if randomise_globals then knuth_shuffle !uninit_global_variable_definitions else !uninit_global_variable_definitions in
  let function_definitions' = if randomise_functions then knuth_shuffle !function_definitions else !function_definitions in
  let gpu_function_definitions' = if randomise_functions then knuth_shuffle !gpu_function_definitions else !gpu_function_definitions in

  (* Now reverse the result and return the resulting file *)
  let rec revonto acc = function
      [] -> acc
    | x :: t -> revonto (x :: acc) t
  in
  f.globals  <- revonto ( revonto ( revonto ( revonto ( revonto (revonto [] function_definitions') gpu_function_definitions') init_global_variable_definitions') uninit_global_variable_definitions' ) ronly_global_variable_definitions' ) !rest_of_globals;
